#ifndef MAINWINDOWWRAPPER_H
#define MAINWINDOWWRAPPER_H

#include <QVBoxLayout>
#include <QWidget>
#include <mainwindow.h>

class MainWindowWrapper : public QWidget {
    Q_OBJECT
public:
    MainWindowWrapper(QWidget* parent = nullptr);
};

#endif // MAINWINDOWWRAPPER_H
