#include "views/dailywidget.h"

DailyWidget::DailyWidget(QWidget* parent)
    : ExpandingWeatherWidget(parent)
{

    day_of_week = new TextWidget(QString(), 16, true, ColorScheme::TRANSPARENT);
    day_of_week->setAlignment(Qt::AlignLeft);
    date = new TextWidget(QString(), 16, true, ColorScheme::TRANSPARENT);
    date->setAlignment(Qt::AlignLeft);
    icon_ = new QLabel;
    min_temp = new TextWidget(tr("Минимум"), 16, true, ColorScheme::TRANSPARENT);
    min_temp->setAlignment(Qt::AlignLeft);
    min_temp_value = new TextWidget(QString(), 16, false, ColorScheme::TRANSPARENT);
    max_temp = new TextWidget(tr("Максимум"), 16, true, ColorScheme::TRANSPARENT);
    max_temp->setAlignment(Qt::AlignLeft);
    max_temp_value = new TextWidget(QString(), 16, false, ColorScheme::TRANSPARENT);

    unhidden_layout->addWidget(day_of_week, 0, 0, 2, 1);
    unhidden_layout->addWidget(date, 2, 0, 2, 1);
    unhidden_layout->addWidget(icon_, 0, 1, 4, 2, Qt::AlignRight);
    unhidden_layout->addWidget(min_temp, 0, 3, 2, 2, Qt::Alignment(Qt::AlignRight));
    unhidden_layout->addWidget(min_temp_value, 0, 5, 2, 1, Qt::Alignment(Qt::AlignRight));
    unhidden_layout->addWidget(max_temp, 2, 3, 2, 2, Qt::Alignment(Qt::AlignRight));
    unhidden_layout->addWidget(max_temp_value, 2, 5, 2, 1, Qt::Alignment(Qt::AlignRight));

    humidity_value = generatePaleLabel();
    clouds_value = generatePaleLabel();
    pressure_value = generatePaleLabel();
    wind_value = generatePaleLabel();
    uv_ix_value = generatePaleLabel();
    visibility_value = generatePaleLabel();
    dew_point_value = generatePaleLabel();
    morn_temp_value = generatePaleLabel();
    morn_fl_value = generatePaleLabel();
    day_temp_value = generatePaleLabel();
    day_fl_value = generatePaleLabel();
    eve_temp_value = generatePaleLabel();
    eve_fl_value = generatePaleLabel();
    night_temp_value = generatePaleLabel();
    night_fl_value = generatePaleLabel();

    hidden_layout->addWidget(new QLabel, 0, 0, 1, 6);
    hidden_layout->addWidget(generateBoldLabel(tr("Давление")), 1, 0, 1, 1);
    hidden_layout->addWidget(pressure_value, 1, 1, 1, 2);
    hidden_layout->addWidget(generateBoldLabel(tr("Ветер")), 1, 3, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(wind_value, 1, 4, 1, 2);
    hidden_layout->addWidget(generateBoldLabel(tr("Влажность")), 3, 0);
    hidden_layout->addWidget(humidity_value, 3, 1);
    hidden_layout->addWidget(generateBoldLabel(tr("Облачность")), 3, 2);
    hidden_layout->addWidget(clouds_value, 3, 3);
    hidden_layout->addWidget(generateBoldLabel(tr("УФ-индекс")), 2, 2);
    hidden_layout->addWidget(uv_ix_value, 2, 3);
    hidden_layout->addWidget(generateBoldLabel(tr("Видимость")), 2, 0);
    hidden_layout->addWidget(visibility_value, 2, 1);
    hidden_layout->addWidget(generateBoldLabel(tr("Точка росы")), 2, 4);
    hidden_layout->addWidget(dew_point_value, 2, 5);
    hidden_layout->addWidget(new QLabel, 4, 0, 1, 6);
    hidden_layout->addWidget(generateBoldLabel(tr("Утром")), 5, 0, 1, 1);
    hidden_layout->addWidget(morn_temp_value, 5, 1, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Ощущается")), 5, 2, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(morn_fl_value, 5, 3, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Днём")), 6, 0, 1, 1);
    hidden_layout->addWidget(day_temp_value, 6, 1, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Ощущается")), 6, 2, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(day_fl_value, 6, 3, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Вечером")), 7, 0, 1, 1);
    hidden_layout->addWidget(eve_temp_value, 7, 1, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Ощущается")), 7, 2, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(eve_fl_value, 7, 3, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Ночью")), 8, 0, 1, 1);
    hidden_layout->addWidget(night_temp_value, 8, 1, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(generateBoldLabel(tr("Ощущается")), 8, 2, 1, 1, Qt::Alignment(Qt::AlignRight));
    hidden_layout->addWidget(night_fl_value, 8, 3, 1, 1, Qt::Alignment(Qt::AlignRight));
}

void DailyWidget::updateWidgetInfo(const DailyModel& model)
{
    day_of_week->setText(model.dayOfWeek());
    date->setText(model.date());
    icon_->setPixmap(model.weatherIcon());

    min_temp_value->setText(model.temperature(QStringLiteral("min")));
    max_temp_value->setText(model.temperature(QStringLiteral("max")));
    pressure_value->setText(model.pressure());
    humidity_value->setText(model.humidity());
    dew_point_value->setText(model.dewPoint());
    uv_ix_value->setText(model.uvi());
    clouds_value->setText(model.cloudness());
    wind_value->setText(model.wind());

    morn_temp_value->setText(model.temperature(QStringLiteral("morning")));
    day_temp_value->setText(model.temperature(QStringLiteral("day")));
    eve_temp_value->setText(model.temperature(QStringLiteral("evening")));
    night_temp_value->setText(model.temperature(QStringLiteral("night")));
    morn_fl_value->setText(model.temperature(QStringLiteral("morning_reelfeel")));
    day_fl_value->setText(model.temperature(QStringLiteral("day_reelfeel")));
    eve_fl_value->setText(model.temperature(QStringLiteral("evening_reelfeel")));
    night_fl_value->setText(model.temperature(QStringLiteral("night_reelfeel")));

    repaint();
}

DailyWidget::~DailyWidget()
{
}
