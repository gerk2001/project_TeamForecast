#ifndef HOURLYWIDGET_H
#define HOURLYWIDGET_H

#include <QJsonObject>
#include <models/hourlymodel.h>
#include <views/expandingweatherwidget.h>

/**
@brief Hourly weather forecast widget
*/

class HourlyWidget : public ExpandingWeatherWidget {
    Q_OBJECT
public:
    HourlyWidget(QWidget* parent = nullptr);
    ~HourlyWidget();
    void updateWidgetInfo(const HourlyModel& model);

private:
    TextWidget* time;
    TextWidget* day;
    TextWidget* temp;
    TextWidget* temp_value;
    TextWidget* feels_like;
    TextWidget* feels_like_value;
    TextWidget* pressure_value;
    TextWidget* humidity_value;
    TextWidget* dew_point_value;
    TextWidget* uv_ix_value;
    TextWidget* clouds_value;
    TextWidget* visibility_value;
    TextWidget* wind_value;
    // QLabel *pop;
    // QLabel *pop_value;
};

#endif // HOURLYWIDGET_H
