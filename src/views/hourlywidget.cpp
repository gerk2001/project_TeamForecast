#include "views/hourlywidget.h"

HourlyWidget::HourlyWidget(QWidget* parent)
    : ExpandingWeatherWidget(parent)
{
    day = new TextWidget(QString(), 16, true, ColorScheme::TRANSPARENT);
    day->setAlignment(Qt::AlignLeft);
    time = new TextWidget(QString(), 16, true, ColorScheme::TRANSPARENT);
    time->setAlignment(Qt::AlignLeft);
    icon_ = new QLabel;
    temp = new TextWidget(tr("Температура"), 16, true, ColorScheme::TRANSPARENT);
    temp->setAlignment(Qt::AlignLeft);
    temp_value = new TextWidget(QString(), 16, false, ColorScheme::TRANSPARENT);
    feels_like = new TextWidget(tr("Ощущается"), 16, true, ColorScheme::TRANSPARENT);
    feels_like->setAlignment(Qt::AlignLeft);
    feels_like_value = new TextWidget(QString(), 16, false, ColorScheme::TRANSPARENT);

    unhidden_layout->addWidget(day, 0, 0, 2, 2);
    unhidden_layout->addWidget(time, 2, 0, 2, 2);
    unhidden_layout->addWidget(icon_, 0, 2, 4, 1, Qt::AlignCenter);
    unhidden_layout->addWidget(temp, 0, 3, 2, 2, Qt::Alignment(Qt::AlignRight));
    unhidden_layout->addWidget(temp_value, 0, 5, 2, 1, Qt::Alignment(Qt::AlignRight));
    unhidden_layout->addWidget(feels_like, 2, 3, 2, 2, Qt::Alignment(Qt::AlignRight));
    unhidden_layout->addWidget(feels_like_value, 2, 5, 2, 1, Qt::Alignment(Qt::AlignRight));

    pressure_value = generatePaleLabel();
    wind_value = generatePaleLabel();
    humidity_value = generatePaleLabel();
    clouds_value = generatePaleLabel();
    dew_point_value = generatePaleLabel();
    visibility_value = generatePaleLabel();
    uv_ix_value = generatePaleLabel();

    hidden_layout->addWidget(new QLabel, 0, 0, 1, 6);
    hidden_layout->addWidget(generateBoldLabel(tr("Давление")), 1, 0, 1, 1);
    hidden_layout->addWidget(pressure_value, 1, 1, 1, 2);
    hidden_layout->addWidget(generateBoldLabel(tr("Ветер")), 1, 3, 1, 1);
    hidden_layout->addWidget(wind_value, 1, 4, 1, 2);
    hidden_layout->addWidget(generateBoldLabel(tr("Влажность")), 2, 0);
    hidden_layout->addWidget(humidity_value, 2, 1);
    hidden_layout->addWidget(generateBoldLabel(tr("Облачность")), 2, 2);
    hidden_layout->addWidget(clouds_value, 2, 3);
    hidden_layout->addWidget(generateBoldLabel(tr("Точка росы")), 2, 4);
    hidden_layout->addWidget(dew_point_value, 2, 5);
    hidden_layout->addWidget(generateBoldLabel(tr("Видимость")), 3, 0);
    hidden_layout->addWidget(visibility_value, 3, 1);
    hidden_layout->addWidget(generateBoldLabel(tr("УФ-индекс")), 3, 2);
    hidden_layout->addWidget(uv_ix_value, 3, 3);
}

void HourlyWidget::updateWidgetInfo(const HourlyModel& model)
{
    time->setText(model.time());
    day->setText(model.day());
    icon_->setPixmap(model.weatherIcon());

    temp_value->setText(model.temperature(QStringLiteral("temperature")));
    feels_like_value->setText(model.temperature(QStringLiteral("reelfeel")));
    pressure_value->setText(model.pressure());
    humidity_value->setText(model.humidity());
    dew_point_value->setText(model.dewPoint());
    uv_ix_value->setText(model.uvi());
    clouds_value->setText(model.cloudness());
    visibility_value->setText(model.visibility());
    wind_value->setText(model.wind());

    repaint();
}

HourlyWidget::~HourlyWidget()
{
}
