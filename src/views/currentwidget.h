#ifndef CURRENTWIDGET_H
#define CURRENTWIDGET_H

#include <QJsonObject>
#include <models/currentmodel.h>
#include <views/expandingweatherwidget.h>

/**
@brief Current weather forecast widget
*/

class CurrentWidget : public ExpandingWeatherWidget {
    Q_OBJECT
public:
    CurrentWidget(QWidget* parent = nullptr);
    ~CurrentWidget();
    void updateWidgetInfo(const CurrentModel& model);

private:
    TextWidget* time; ///< current time field
    // QLabel *sunrise;
    // QLabel *sunset;
    TextWidget* temp; ///< current temp label
    TextWidget* temp_value; ///< current temp value field
    TextWidget* feels_like; ///< current feels like label
    TextWidget* feels_like_value; ///< current feels like value field

    TextWidget* pressure_value; ///< current pressure value field
    TextWidget* humidity_value; ///< current humidity value field
    TextWidget* dew_point_value; ///< current dew-point value field
    TextWidget* uv_ix_value; ///< current UV-index value field
    TextWidget* clouds_value; ///< current cloudness value field
    TextWidget* visibility_value; ///< current visibility value field
    TextWidget* wind_value; ///< current wind speed and direction values field
};

#endif // CURRENTWIDGET_H
