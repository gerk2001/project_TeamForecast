#include "views/expandingweatherwidget.h"

ExpandingWeatherWidget::ExpandingWeatherWidget(QWidget* parent)
    : AnimatedWidget(parent)
{
    setColorScheme(ColorScheme::PRIMARY);
    setRadius(AppStyle::biggestCornerRadius());
    auto main_layout = new QVBoxLayout;
    main_layout->setSpacing(0);
    setLayout(main_layout);
    main_layout->setContentsMargins(4, 4, 4, 4);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

    unhidden_frame = new QWidget;
    unhidden_layout = new QGridLayout;
    unhidden_frame->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    unhidden_frame->setLayout(unhidden_layout);

    hidden_frame = new QWidget;
    hidden_layout = new QGridLayout;
    hidden_frame->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Ignored);
    hidden_frame->setLayout(hidden_layout);
    QSizePolicy sp_retain = hidden_frame->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    hidden_frame->setSizePolicy(sp_retain);
    hidden_frame->setFixedHeight(0);

    main_layout->addWidget(unhidden_frame);
    main_layout->addWidget(hidden_frame);
}

ExpandingWeatherWidget::~ExpandingWeatherWidget()
{
}

void ExpandingWeatherWidget::animateWidget(QPoint start_pos, int duration)
{
    if (previous_state_ == STATE::HOVERED) {
        auto arect = rect();

        animation = new QPropertyAnimation(this, "hidden_height");
        animation->setDuration(duration);
        if (auto height = hidden_frame->height(); height == 0) {
            animation->setStartValue(0);
            animation->setEndValue(hidden_frame->sizeHint().height());
            arect.setHeight(arect.height() + hidden_frame->sizeHint().height());
        } else {
            animation->setStartValue(height);
            animation->setEndValue(0);
            arect.setHeight(arect.height() - height);
        }
        connect(animation,
            &QPropertyAnimation::finished,
            this,
            [=]() {
                if (hidden_frame->height())
                    hidden_frame->show();
                else
                    hidden_frame->hide();
            });

        arect_anim_ = new QPropertyAnimation(this, "arect_");
        arect_anim_->setDuration(duration);
        arect_anim_->setStartValue(QRect(start_pos, QSize(0, 0)));
        arect_anim_->setEasingCurve(QEasingCurve::OutQuad);
        arect_anim_->setEndValue(arect);

        auto anim_group = new QParallelAnimationGroup(this);
        anim_group->addAnimation(arect_anim_);
        anim_group->addAnimation(animation);
        anim_group->start(QAbstractAnimation::DeleteWhenStopped);
    }
}
