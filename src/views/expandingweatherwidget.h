#ifndef EXPANDINGWEATHERWIDGET_H
#define EXPANDINGWEATHERWIDGET_H

#include <QDateTime>
#include <QGridLayout>
#include <QPainter>
#include <QPropertyAnimation>
#include <QStyleOption>
#include <QVBoxLayout>
#include <QWidget>
#include <base/animatedwidget.h>
#include <base/appstyle.h>
#include <base/modifiedqlabel.h>
#include <base/textwidget.h>

/**
@brief Base widget for Current-, Hourly- and Daily- weather forecast widgets.
@details Provides an opportunity to expand/collapse content
placed in hidden_layout for all inheriting it classes.\n
Also contains some helper functions like getWindDirection(const int &degrees)
and getWeatherIcon(const int &weather_id)
*/

class ExpandingWeatherWidget : public AnimatedWidget {
    Q_OBJECT
    Q_PROPERTY(int hidden_height READ getHeight WRITE setHeight) ///< hidden_frame height macro
public:
    /**
    @brief Constructs new ExpandingWeatherWidget
    @param parent Parent for this widget
    */
    ExpandingWeatherWidget(QWidget* parent = nullptr);
    ~ExpandingWeatherWidget();
    void updateIconColor()
    {
        if (icon_)
            icon_->setPixmap(AppStyle::transformPixmap(icon_->pixmap()));
    }

protected:
    TextWidget* generateBoldLabel(const QString& text)
    {
        auto label = new TextWidget(text, 11, true, ColorScheme::TRANSPARENT);
        label->setAlignment(Qt::AlignLeft);
        return label;
    }
    TextWidget* generatePaleLabel()
    {
        auto label = new TextWidget(QString(), 11, false, ColorScheme::TRANSPARENT);
        label->setAlignment(Qt::AlignLeft);
        return label;
    }
    QGridLayout* unhidden_layout; ///< Layout that contains always visible elements
    QGridLayout* hidden_layout; ///< Layout that contains elements which can be hidden
    QLabel* icon_; ///< Icon of weather state
    virtual void animateWidget(QPoint start_pos, int duration) override;

private:
    QWidget* hidden_frame; ///< Widget containing hidden_layout
    QWidget* unhidden_frame; ///< Widget containing unhidden_layout
    QPropertyAnimation* animation; ///< Expand/collapse hidden_frame animation object
    /**
    @brief setter for Q_PROPERTY hidden_height macro
    @return current hidden_frame height
    */
    int getHeight()
    {
        return hidden_frame->height();
    }
    /**
    @brief setter for Q_PROPERTY hidden_height macro
    @param value New hidden_frame height
    */
    void setHeight(int value)
    {
        hidden_frame->setFixedHeight(value);
        update();
    }
};

#endif // EXPANDINGWEATHERWIDGET_H
