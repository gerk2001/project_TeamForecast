#ifndef DAILYWIDGET_H
#define DAILYWIDGET_H

#include <QJsonObject>
#include <models/dailymodel.h>
#include <views/expandingweatherwidget.h>

/**
@brief Daily weather forecast widget
*/

class DailyWidget : public ExpandingWeatherWidget {
    Q_OBJECT
public:
    DailyWidget(QWidget* parent = nullptr);
    ~DailyWidget();
    void updateWidgetInfo(const DailyModel& model);

private:
    TextWidget* day_of_week;
    TextWidget* date;
    // QLabel *sunrise;
    // QLabel *sunset;
    TextWidget* min_temp;
    TextWidget* min_temp_value;
    TextWidget* max_temp;
    TextWidget* max_temp_value;

    TextWidget* pressure_value;
    TextWidget* humidity_value;
    TextWidget* dew_point_value;
    TextWidget* uv_ix_value;
    TextWidget* clouds_value;
    TextWidget* visibility_value;
    TextWidget* wind_value;

    TextWidget* morn_temp_value;
    TextWidget* day_temp_value;
    TextWidget* eve_temp_value;
    TextWidget* night_temp_value;
    TextWidget* morn_fl_value;
    TextWidget* day_fl_value;
    TextWidget* eve_fl_value;
    TextWidget* night_fl_value;
};

#endif // DAILYWIDGET_H
