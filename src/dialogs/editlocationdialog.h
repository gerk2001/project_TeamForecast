#ifndef EDITLOCATIONDIALOG_H
#define EDITLOCATIONDIALOG_H

#include <dialogs/basiclocationdialog.h>
#include <views/locationforecast.h>

/**
@brief Modal dialog used for editing data of the location widget from which it was called
@details If ok_btn is clicked then edits this location widget
with entered in this dialog label, latitude and longitude.\n
If need_update was switched to true (latitude or longitude were changed) after
this dialog is opened, then location widget with edited data updates after dialog closes
*/

class EditLocationDialog : public BasicLocationDialog {
	Q_OBJECT
public:
    /**
    @brief Constructs new modal EditLocationDialog
    @param parent Parent for this dialog
    @param location_model LocationModel that should be edited
    @param dimmable_widget Widget to which will be applied DarkOverlayEffect
    */
    EditLocationDialog(QWidget* parent,
        LocationModel& location_model,
        QWidget* dimmable_widget = nullptr);
    virtual ~EditLocationDialog();
};

#endif // EDITLOCATIONDIALOG_H
