#include "mainwindowwrapper.h"

MainWindowWrapper::MainWindowWrapper(QWidget* parent)
    : QWidget(parent, Qt::FramelessWindowHint)
{
    setAttribute(Qt::WA_TranslucentBackground);
    setMinimumHeight(600);

    auto layout = new QVBoxLayout();
    setLayout(layout);

    auto window = new MainWindow(this);
    layout->addWidget(window);
}
