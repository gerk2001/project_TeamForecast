#include "models/dailymodel.h"

DailyModel::DailyModel()
    : AbstractWeatherModel()
{
}

void DailyModel::updateModel(const QVariantMap& new_data, int offset)
{
    data[QStringLiteral("date")] =
        QDateTime::fromSecsSinceEpoch(new_data[QStringLiteral("dt")].toLongLong(), Qt::OffsetFromUTC, offset).date();

    auto temp = new_data[QStringLiteral("temp")].toMap();
    data[QStringLiteral("min")] = temp[QStringLiteral("min")];
    data[QStringLiteral("max")] = temp[QStringLiteral("max")];
    data[QStringLiteral("morning")] = temp[QStringLiteral("morn")];
    data[QStringLiteral("day")] = temp[QStringLiteral("day")];
    data[QStringLiteral("evening")] = temp[QStringLiteral("eve")];
    data[QStringLiteral("night")] = temp[QStringLiteral("night")];

    auto reelfeel = new_data[QStringLiteral("feels_like")].toMap();
    data[QStringLiteral("morning_reelfeel")] = reelfeel[QStringLiteral("morn")];
    data[QStringLiteral("day_reelfeel")] = reelfeel[QStringLiteral("day")];
    data[QStringLiteral("evening_reelfeel")] = reelfeel[QStringLiteral("eve")];
    data[QStringLiteral("night_reelfeel")] = reelfeel[QStringLiteral("night")];

    data[QStringLiteral("pressure")] = new_data[QStringLiteral("pressure")];
    data[QStringLiteral("humidity")] = new_data[QStringLiteral("humidity")];
    data[QStringLiteral("dew_point")] = new_data[QStringLiteral("dew_point")];
    data[QStringLiteral("wind_speed")] = new_data[QStringLiteral("wind_speed")];
    data[QStringLiteral("wind_deg")] = new_data[QStringLiteral("wind_deg")];
    data[QStringLiteral("cloudness")] = new_data[QStringLiteral("clouds")];
    data[QStringLiteral("uvi")] = new_data[QStringLiteral("uvi")];
    data[QStringLiteral("weather_ID")] = new_data[QStringLiteral("weather")].toList()[0].toMap()[QStringLiteral("id")].toInt();
}
