#ifndef DAILYMODEL_H
#define DAILYMODEL_H

#include "models/abstractweathermodel.h"

class DailyModel : public AbstractWeatherModel {
public:
    DailyModel();
    QString visibility() = delete;
    void updateModel(const QVariantMap &new_data, int offset);
    QString date() const { return data[QStringLiteral("date")].toDate().toString(QStringLiteral("dd.MM")); }
    QString dayOfWeek() const { return QLocale::system().dayName(data[QStringLiteral("date")].toDate().dayOfWeek(), QLocale::ShortFormat); }
};

#endif // DAILYMODEL_H
