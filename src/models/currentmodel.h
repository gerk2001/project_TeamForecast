#ifndef CURRENTMODEL_H
#define CURRENTMODEL_H

#include "models/abstractweathermodel.h"

class CurrentModel : public AbstractWeatherModel {
public:
    CurrentModel();
    void updateModel(QVariantMap& new_data, int offset);
    QString time() const { return data[QStringLiteral("time")].toTime().toString(QStringLiteral("hh:mm")); };
};

#endif // CURRENTMODEL_H
