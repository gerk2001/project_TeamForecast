#include "models/currentmodel.h"

CurrentModel::CurrentModel()
    : AbstractWeatherModel()
{
}

void CurrentModel::updateModel(QVariantMap& new_data, int offset)
{
    data[QStringLiteral("time")] =
        QDateTime::fromSecsSinceEpoch(new_data[QStringLiteral("dt")].toLongLong(),
                                                                 Qt::OffsetFromUTC,
                                                                 offset).time();
    data[QStringLiteral("temperature")] = new_data[QStringLiteral("temp")];
    data[QStringLiteral("reelfeel")] = new_data[QStringLiteral("feels_like")];
    data[QStringLiteral("pressure")] = new_data[QStringLiteral("pressure")];
    data[QStringLiteral("humidity")] = new_data[QStringLiteral("humidity")];
    data[QStringLiteral("dew_point")] = new_data[QStringLiteral("dew_point")];
    data[QStringLiteral("wind_speed")] = new_data[QStringLiteral("wind_speed")];
    data[QStringLiteral("wind_deg")] = new_data[QStringLiteral("wind_deg")];
    data[QStringLiteral("cloudness")] = new_data[QStringLiteral("clouds")];
    data[QStringLiteral("uvi")] = new_data[QStringLiteral("uvi")];
    data[QStringLiteral("weather_ID")] = new_data[QStringLiteral("weather")].toList()[0].toMap()[QStringLiteral("id")].toInt();
    data[QStringLiteral("visibility")] = new_data[QStringLiteral("visibility")];
}
