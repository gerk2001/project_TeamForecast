#include "models/hourlymodel.h"

HourlyModel::HourlyModel()
    : AbstractWeatherModel()
{
}

void HourlyModel::updateModel(const QVariantMap& new_data, const QDateTime& today, int offset)
{
    data[QStringLiteral("today")] = today;
    data[QStringLiteral("date_time")] =
        QDateTime::fromSecsSinceEpoch(new_data[QStringLiteral("dt")].toLongLong(), Qt::OffsetFromUTC, offset);
    data[QStringLiteral("temperature")] = new_data[QStringLiteral("temp")];
    data[QStringLiteral("reelfeel")] = new_data[QStringLiteral("feels_like")];
    data[QStringLiteral("pressure")] = new_data[QStringLiteral("pressure")];
    data[QStringLiteral("humidity")] = new_data[QStringLiteral("humidity")];
    data[QStringLiteral("dew_point")] = new_data[QStringLiteral("dew_point")];
    data[QStringLiteral("wind_speed")] = new_data[QStringLiteral("wind_speed")];
    data[QStringLiteral("wind_deg")] = new_data[QStringLiteral("wind_deg")];
    data[QStringLiteral("cloudness")] = new_data[QStringLiteral("clouds")];
    data[QStringLiteral("uvi")] = new_data[QStringLiteral("uvi")];
    data[QStringLiteral("weather_ID")] = new_data[QStringLiteral("weather")].toList()[0].toMap()["id"].toInt();
    data[QStringLiteral("visibility")] = new_data[QStringLiteral("visibility")];
}

QString HourlyModel::day() const
{
    if (auto diff = today().daysTo(data[QStringLiteral("date_time")].toDateTime()); diff == 0)
        return QObject::tr("Сегодня");
    else if (diff == 1)
        return QObject::tr("Завтра");
    else
        return QObject::tr("Послезавтра");
}
