#include "base/appstyle.h"

#include <utility>
#include <QPainter>

AppStyle::AppStyle()
{
}

void AppStyle::setTheme(const QString &theme)
{
    current_theme_ = theme;
}

QColor AppStyle::backgroundColor(ColorScheme color_scheme, STATE state)
{
    return background_colors[current_theme_][color_scheme][state];
}

void AppStyle::transformPixmap(QPixmap* source)
{
    QPainter painter(source);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.setBrush(textPrimaryColor());
    painter.setPen(Qt::NoPen);
    painter.drawRect(source->rect());
}

QPixmap AppStyle::transformPixmap(QPixmap source)
{
    transformPixmap(&source);
    return source;
}

QString AppStyle::current_theme_ =QStringLiteral("default_day");
double AppStyle::biggest_corner_radius_ = 20;
double AppStyle::medium_corner_radius_ = 16;
double AppStyle::smallest_corner_radius_ = 10;

QHash<QString, QHash<ColorScheme, QHash<STATE, QColor>>> AppStyle::background_colors = {
    { "default_day",
        { { ColorScheme::PRIMARY,
              { { STATE::NORMAL, QColor(255, 176, 76) },
                  { STATE::FOCUSED, QColor(255, 176, 76) },
                  { STATE::HOVERED, QColor(245, 127, 23) },
                  { STATE::PRESSED, QColor(188, 81, 0) } } },
            { ColorScheme::SECONDARY,
                { { STATE::NORMAL, QColor(255, 255, 255) },
                    { STATE::FOCUSED, QColor(224, 224, 224) },
                    { STATE::HOVERED, QColor(224, 224, 224) },
                    { STATE::PRESSED, QColor(174, 174, 174) } } },
            { ColorScheme::TRANSPARENT,
                { { STATE::NORMAL, QColor(0, 0, 0, 0) },
                    { STATE::FOCUSED, QColor(0, 0, 0, 0) },
                    { STATE::HOVERED, QColor(0, 0, 0, 0) },
                    { STATE::PRESSED, QColor(0, 0, 0, 0) } } } } },
    { "default_night",
        { { ColorScheme::PRIMARY,
              { { STATE::NORMAL, QColor(1, 87, 155) },
                  { STATE::FOCUSED, QColor(79, 131, 204) },
                  { STATE::HOVERED, QColor(79, 131, 204) },
                  { STATE::PRESSED, QColor(0, 47, 108) } } },
            { ColorScheme::SECONDARY,
                { { STATE::NORMAL, QColor(55, 71, 79) },
                    { STATE::FOCUSED, QColor(98, 114, 123) },
                    { STATE::HOVERED, QColor(98, 114, 123) },
                    { STATE::PRESSED, QColor(45, 57, 64) } } },
            { ColorScheme::TRANSPARENT,
                { { STATE::NORMAL, QColor(0, 0, 0, 0) },
                    { STATE::FOCUSED, QColor(0, 0, 0, 0) },
                    { STATE::HOVERED, QColor(0, 0, 0, 0) },
                    { STATE::PRESSED, QColor(0, 0, 0, 0) } } } } }
};

const QHash<QString, QColor> AppStyle::text_primary_colors_ = {
    { "default_day", QColor(0, 0, 0) },
    { "default_night", QColor(255, 255, 255) }
};
