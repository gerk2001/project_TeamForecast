#ifndef TEXTWIDGET_H
#define TEXTWIDGET_H

#include <QPainter>
#include <base/appstyle.h>
#include <base/basewidget.h>

class TextWidget : public BaseWidget
{
    Q_OBJECT
public:
    explicit TextWidget(const QString & text = QString(),
                        int size = 10,
                        bool bold = false,
                        ColorScheme color_scheme = ColorScheme::PRIMARY,
                        QWidget *parent = nullptr);
    void setText(const QString& text)
    {
        text_ = text;
        adjustSize();
        update();
    }
    QString text() const { return text_; }
    void setFontParams(int font_size, bool bold = false);
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;
    void setAlignment(int qt_align_flag) { align_ = qt_align_flag; }

protected:
    virtual void paintEvent(QPaintEvent* event) override;

private:
    QString text_;
    ColorScheme color_scheme_;
    int align_;
};

#endif // TEXTWIDGET_H
