#ifndef ANIMATEDWIDGET_H
#define ANIMATEDWIDGET_H

#include <QMouseEvent>
#include <QPainter>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QWidget>
#include <base/appstyle.h>
#include <base/basewidget.h>

class AnimatedWidget : public BaseWidget {
    Q_OBJECT
    Q_PROPERTY(int edge_r_ READ edgeR WRITE setEdgeR)
    Q_PROPERTY(int edge_l_ READ edgeL WRITE setEdgeL)
    Q_PROPERTY(QRect arect_ READ arect WRITE setArect())
    Q_PROPERTY(bool selectable_ READ selectable WRITE setSelectable)

public:
    explicit AnimatedWidget(QWidget* parent = nullptr);
    int edge_r_;
    int edge_l_;
    bool from_left_;
    bool from_right_;
    int start_x_r_;
    int start_x_l_;
    int edgeR() const;
    void setEdgeR(int edge_r);
    int edgeL() const;
    void setEdgeL(int edge_l);
    bool selectable() const { return selectable_; }
    void setSelectable(bool selectable) { selectable_ = selectable; }
    void setArect(const QRect& rect)
    {
        arect_ = rect;
        update();
    }
    QRect arect() const { return arect_; }
signals:
    void hovered();
    void pressed();
    void released();
    void clicked();

protected:
    STATE previous_state_;
    bool selectable_;
    QPropertyAnimation* arect_anim_;
    virtual void paintEvent(QPaintEvent* event) override;
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;
    virtual void enterEvent(QEnterEvent* event) override;
    virtual void leaveEvent(QEvent* event) override;
    virtual void focusInEvent(QFocusEvent* event) override;
    virtual void focusOutEvent(QFocusEvent* event) override;
    virtual void animateWidget(QPoint start_pos, int duration = 150);

private:
    QRect arect_;
};

#endif // ANIMATEDWIDGET_H
