#include "base/textwidget.h"

TextWidget::TextWidget(
    const QString &text, int size, bool bold, ColorScheme color_scheme, QWidget *parent)
    : BaseWidget(parent)
{
    if (text.size() > 0)
        setText(text);
    setColorScheme(color_scheme);
    setFontParams(size, bold);
    setAlignment(Qt::AlignCenter);
}

void TextWidget::setFontParams(int font_size, bool bold)
{
    auto current_font = font();
    current_font.setBold(bold);
    current_font.setPointSize(font_size);
    setFont(current_font);
}

QSize TextWidget::sizeHint() const
{
    int h = 0;
    int w = 0;
    QString str(text_);
    bool empty = str.isEmpty();
    if (empty)
        str = QStringLiteral("XXXX");
    QFontMetrics fm = fontMetrics();
    QSize str_size = fm.size(Qt::TextShowMnemonic, str);
    if (!empty || !w)
        w = str_size.width();
    if (!empty || !h)
        h = str_size.height();
    return QSize(w, h);
}

QSize TextWidget::minimumSizeHint() const
{
    return sizeHint();
}

void TextWidget::paintEvent(QPaintEvent* event)
{
    BaseWidget::paintEvent(event);
    QPainter painter(this);
    painter.setRenderHint(QPainter::TextAntialiasing);
    painter.setBrush(QBrush(AppStyle::backgroundColor(color_scheme_, STATE::NORMAL)));
    painter.setPen(QPen(AppStyle::textPrimaryColor()));
    painter.drawText(rect(), align_, text_);
}
