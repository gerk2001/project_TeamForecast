#include "modifiedqlabel.h"
#include <QPainter>

ModifiedQLabel::ModifiedQLabel(QColor icon_color, QWidget* parent)
    : QLabel(parent)
{
    current_color_ = icon_color;
}

void ModifiedQLabel::paintEvent(QPaintEvent* event)
{
    if (current_color_ != AppStyle::textPrimaryColor()) {
        current_color_ = AppStyle::textPrimaryColor();
        auto icon = pixmap();
        QPainter painter(&icon);
        painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
        painter.setBrush(current_color_);
        painter.setPen(Qt::NoPen);
        painter.drawRect(icon.rect());
        setPixmap(icon);
    }
    QLabel::paintEvent(event);
}
