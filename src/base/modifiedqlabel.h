#ifndef MODIFIEDQLABEL_H
#define MODIFIEDQLABEL_H

#include <QLabel>
#include <base/appstyle.h>

class ModifiedQLabel : public QLabel {
	Q_OBJECT
public:
    ModifiedQLabel(QColor icon_color, QWidget* parent = nullptr);

private:
    QColor current_color_;

protected:
    virtual void paintEvent(QPaintEvent* event) override;
};

#endif // MODIFIEDQLABEL_H
