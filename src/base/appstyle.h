#ifndef APPSTYLE_H
#define APPSTYLE_H
#include <QColor>
#include <QHash>
#include <QPixmap>
#include <QString>

enum STATE : char {
    NORMAL,
    HOVERED,
    FOCUSED,
    PRESSED,
    SELECTED
};

enum ColorScheme : char {
    PRIMARY,
    SECONDARY,
    MENU,
    TRANSPARENT
};

using color_table = QHash<QString, QHash<ColorScheme, QHash<STATE, QColor>>>;

class AppStyle {
public:
    AppStyle();
    static void setTheme(const QString &theme);
    static QString theme() { return current_theme_; }
    static double biggestCornerRadius() { return biggest_corner_radius_; }
    static double mediumCornerRadius() { return medium_corner_radius_; }
    static double smallestCornerRadius() { return smallest_corner_radius_; }
    static QColor backgroundColor(ColorScheme color_scheme, STATE state);
    static void transformPixmap(QPixmap* source);
    static QPixmap transformPixmap(QPixmap source);
    static QColor textPrimaryColor()
    {
        return text_primary_colors_[current_theme_];
    }

private:
    static QString current_theme_;
    static double biggest_corner_radius_;
    static double medium_corner_radius_;
    static double smallest_corner_radius_;
    static color_table background_colors;
    static color_table content_colors;
    static const QHash<QString, QColor> text_primary_colors_;
};

#endif // APPSTYLE_H
