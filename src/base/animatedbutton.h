#ifndef ANIMATEDBUTTON_H
#define ANIMATEDBUTTON_H

#include "base/animatedwidget.h"
#include <QShortcut>

class AnimatedButton : public AnimatedWidget {
	Q_OBJECT
public:
    AnimatedButton(const QString &text = QString(),
                   ColorScheme color_scheme = ColorScheme::PRIMARY,
                   int radius = AppStyle::mediumCornerRadius(),
                   QWidget *parent = nullptr);
    void setText(const QString& text)
    {
        text_ = text;
        update();
    }
    QString text() const { return text_; }
    void setFontParams(int font_size, bool bold = false);
    QSize sizeHint() const override;
    void setLayout(QLayout* layout) = delete;
    void setShortcut(const QKeySequence& key)
    {
        if (shortcut_)
            shortcut_->deleteLater();
        shortcut_ = new QShortcut(key, this);
        connect(shortcut_, &QShortcut::activated, [this]() { if (isEnabled()) emit clicked(); });
    }
    auto shortcut() { return shortcut_; }

protected:
    virtual void paintEvent(QPaintEvent* event) override;

private:
    QString text_;
    QShortcut* shortcut_;
};

#endif // ANIMATEDBUTTON_H
