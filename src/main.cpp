#include <QApplication>
#include <mainwindowwrapper.h>

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    MainWindowWrapper w;
    w.show();
    return a.exec();
}
