#ifndef NOTIFICATIONSYSTEM_H
#define NOTIFICATIONSYSTEM_H

#include <QApplication>
#include <QHBoxLayout>
#include <QObject>
#include <QTimer>
#include <base/animatedbutton.h>
#include <base/basewidget.h>
#include <base/textwidget.h>

class NotificationSystem : public QObject {
    Q_OBJECT
public:
    explicit NotificationSystem(QObject* parent = nullptr);
    static void showToast(const QString& message, int duration = 4000);
    static void initializeRect(QRect working_rect) { working_rect_ = working_rect; };

private:
    static QRect working_rect_;
};

#endif // NOTIFICATIONSYSTEM_H
