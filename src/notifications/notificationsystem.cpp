#include "notificationsystem.h"

QRect NotificationSystem::working_rect_ = QRect(0, 0, 0, 0);

NotificationSystem::NotificationSystem(QObject* parent)
    : QObject(parent)
{
}

void NotificationSystem::showToast(const QString& message, int duration)
{
    auto toast = new BaseWidget;
    toast->setWindowFlags(
        Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::Tool | Qt::NoDropShadowWindowHint);
    toast->setAttribute(Qt::WA_TranslucentBackground);
    auto layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    toast->setLayout(layout);

    auto message_view = new TextWidget(message, 12, false);
    message_view->setMinimumWidth(message_view->sizeHint().width() * 1.5);
    auto close_btn = new AnimatedButton(tr("Закрыть"), ColorScheme::PRIMARY, 0);
    close_btn->setFontParams(12);
    close_btn->setMinimumWidth(close_btn->sizeHint().width() * 1.5);
    connect(close_btn, &AnimatedButton::clicked, toast, &QWidget::deleteLater);

    layout->addWidget(message_view);
    layout->addWidget(close_btn);
    toast->adjustSize();
    toast->setFixedHeight(toast->sizeHint().height() * 1.2);

    toast->move(
        toast->mapToGlobal(
            QPoint(
                working_rect_.center().x() - toast->width() / 2,
                working_rect_.bottom() - toast->sizeHint().height() * 2)));
    toast->show();

    QTimer::singleShot(duration, toast, &QWidget::deleteLater);
}
